# taoke-wallet

#### 介绍
taoke-wallet一个淘宝客系统与数字货币钱包相结合的购物返利系统，包含二级分销的淘客系统、金融级的账务系统、数字货币钱包提取返利token积分，目前支持BTC、ETH等token积分。本项目为平台的前端工程，后台架构代码开源（见下面技术选型GitHub链接），业务代码请扫描二维码咨询：
![微信二维码](https://images.gitee.com/uploads/images/2020/0730/113131_7a29fb20_5250257.jpeg "微信二维码")

#### 软件架构
软件架构说明

##### 后台技术选型

| 技术                   | 说明                                    |
| ---------------------- | --------------------------------------- |
| Spring Boot            | MVC核心框架                             |
| Spring Security        | 认证和授权框架                          |
| MyBatis                | ORM框架                                 |

基于以上基础技术，深度自定义金融级平台架构。


后台架构代码见GitHub：
[https://github.com/JolyHuang/cube.git](https://github.com/JolyHuang/cube.git)
[https://github.com/JolyHuang/boot.git](https://github.com/JolyHuang/boot.git)

##### 前端技术选型

uniapp:使用 Vue.js 开发跨平台应用的前端框架


#### 使用说明
![首页](https://images.gitee.com/uploads/images/2020/0730/112321_5e8f090b_5250257.jpeg "1.jpg")
![首页2](https://images.gitee.com/uploads/images/2020/0730/112341_f27c307c_5250257.jpeg "2.jpg")
![优选](https://images.gitee.com/uploads/images/2020/0730/112400_c705e237_5250257.jpeg "3.jpg")
![搜索](https://images.gitee.com/uploads/images/2020/0730/112420_d00ec56c_5250257.jpeg "4.jpg")
![钱包](https://images.gitee.com/uploads/images/2020/0730/112438_12bfbd5a_5250257.jpeg "5.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112502_56ca7307_5250257.jpeg "6.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112517_7b6cdacb_5250257.jpeg "7.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112534_e90aeae1_5250257.jpeg "8.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112548_a8014b6f_5250257.jpeg "9.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112602_ab2e1ba4_5250257.jpeg "10.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112616_d07f6640_5250257.jpeg "11.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112630_ed3d9cf2_5250257.jpeg "12.jpg")
![](https://images.gitee.com/uploads/images/2020/0730/112644_0e50fa2e_5250257.jpeg "13.jpg")
