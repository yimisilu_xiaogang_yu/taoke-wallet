import Vue from 'vue'
import App from './App'

/* 导航栏 */
import navBar from "@/components/nav-bar.vue"
Vue.component('nav-bar', navBar)

/* 对话框 */
import dialogBox from "@/components/dialog-box.vue"
Vue.component('dialog-box', dialogBox)

/* 搜索剪切板内容 */
//#ifdef APP-PLUS
import searchclipboard from "@/components/search-clipboard.vue"
Vue.component('search-clipboard', searchclipboard)
// #endif

/* 图标组件 */
import iconFont from "@/components/icon-font.vue"
Vue.component('icon-font', iconFont)

//蒙层
import loading from "@/components/loading.vue"
Vue.component('loading', loading)

// 注册分页组件
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue"
Vue.component('mescroll-body', MescrollBody)
import MescrollUni from "@/components/mescroll-uni/mescroll-uni.vue"
Vue.component('mescroll-uni', MescrollUni)

//vuex
import store from '@/store'

//请求封装 by elinor
import http from "@/common/http.js"
Vue.prototype.$http = http

//验证工具类封装 by elinor
import util from "@/common/util.js"
Vue.prototype.$util = util

//页面键传值监听
import OnFire from "@/common/onFire.js"
Vue.prototype.$onFire = new OnFire()


//时间戳过滤器
Vue.filter('formatDate', function(value) {
	let tmp = '';
	if (value) {
		let date = new Date(value);
		let y = date.getFullYear();
		let MM = date.getMonth() + 1;
		MM = MM < 10 ? '0' + MM : MM;
		let d = date.getDate();
		d = d < 10 ? '0' + d : d;
		let h = date.getHours();
		h = h < 10 ? '0' + h : h;
		let m = date.getMinutes();
		m = m < 10 ? '0' + m : m;
		let s = date.getSeconds();
		s = s < 10 ? '0' + s : s;
		tmp = y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s;
	}
	return tmp;
})

//时间戳过滤器短日期
Vue.filter('formatShortDate', function(value) {
	let tmp = '';
	if (value) {
		let date = new Date(value);
		let y = date.getFullYear();
		let MM = date.getMonth() + 1;
		MM = MM < 10 ? '0' + MM : MM;
		let d = date.getDate();
		d = d < 10 ? '0' + d : d;
		tmp = y + '-' + MM + '-' + d ;
	}
	return tmp;
})

//字段空过滤器
Vue.filter('isNull', function(value) {
	return value==null?'':value;
})

//保留两位小数的过滤器
Vue.filter('Fixed2', function(value) {
	let tmp = parseFloat(value).toFixed(2);
	return isNaN(tmp) ? '0.00' : tmp;
})

//按BTC过滤保留8小数的过滤器
Vue.filter('FixedBTC', function(value) {
	let tmp = parseFloat(value).toFixed(8);
	return isNaN(tmp) ? '0.00000000' : tmp;
})

//按ETH过滤保留6小数的过滤器
Vue.filter('FixedETH', function(value) {
	let tmp = parseFloat(value).toFixed(6);
	return isNaN(tmp) ? '0.000000' : tmp;
})

Vue.filter('commissionCoupon', function(commission,coupon) {
	//计算立即购买省多少钱 劵+佣金
	let cm = commission != null ? parseFloat(commission) : 0;
	let cu = coupon != null ? parseFloat(coupon) : 0;
	return util.FixedCeil2(cm + cu);
})

Vue.filter('shopType', function(type) {
	//店铺类型
	return type == '0' ? '淘宝' : '天猫';
})





Vue.config.productionTip = false

Vue.prototype.$store = store;
App.mpType = 'app'
const app = new Vue({
    ...App,
	store
})
app.$mount()