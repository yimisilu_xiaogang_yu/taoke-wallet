import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		searchDialog: false,
		searchContent: '',
		searchTBStorageHistory: 'tbhistory',
		iosUrl: '',
		androidUrl: '',
		version_update: 100,
		version_force_update: false,
		version: 100,
		checkMode: false, //审核模式
	},
	mutations: {
		//隐藏对话框
		hideSearchDialog(state) {
			state.searchDialog = false;
			state.searchContent = '';
		},
		//显示对话框并复制剪切板内容
		showSearchDialog(state, content) {
			state.searchDialog = true;
			state.searchContent = content;
		},
		//显示对话框
		setSearchDialog(state) {
			state.searchDialog = true;
		},
		// 设置api 应用商店下载地址
		setAPIUrl(state, list) {
			list.forEach(item => {
				if (item.parameterName == "ios") {
					state.iosUrl = item.parameterValue;
				} else if (item.parameterName == "android") {
					state.androidUrl = item.parameterValue;
				}
			})
		},
		// 设置api 后台的版本号
		setAPIVersion(state, list) {
			list.forEach(item => {
				if (item.parameterName == "version_update") {
					state.version_update = parseInt(item.parameterValue.split('.').join(''));
					//state.version_update =102;
				} else if (item.parameterName == "version_force_update") {
					state.version_force_update = item.parameterValue == 'true';
				}
			})
		},
		//设置是否为审核模式
		setCheckMode(state, data) {
			// #ifdef APP-PLUS
			const sys = uni.getSystemInfoSync();
			let osName = sys.platform == 'ios' ? 'ios_online_switch' : 'android_online_switch';
			let item = data.find(tmp => tmp.parameterName == osName)
			//console.log('设置是否为审核模式', data, state.version, osName, item)
			if (item) {
				if (item.parameterValue == "open") {
					let checkvision = parseInt(item.version.split('.').join(''));
					state.checkMode = state.version == checkvision;
				}
			}
			// #endif
		},
		// 设置当前的版本号
		setVersion(state, v) {
			state.version = v;
		},
		//修改强制更新值
		updateVesion(state) {
			state.version_force_update = false;
			state.version_update = 100;
		}
	},
	actions: {

	}
})

export default store
